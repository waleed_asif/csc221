#ifndef TILECONVERTER_H
#define TILECONVERTER_H

#include <QtGui>
#include <QtCore>


class TileConverter
{
public:
    TileConverter();
    TileConverter(const TileConverter &);
    TileConverter& operator=(const TileConverter &);

    void reset();
    void setOriginalImage(const QImage&);
    void setImage(const QImage&);
    const QImage& ScanImage	(const QImage &image);
    const QImage& originalImage() const;
    const QImage& tiledImage() const;
    void computeAvgTile();

protected:
    QImage m_bestTile;
    QImage m_origImage;
    QImage m_outImage;
    QMap<QString, int> m_FilePathDB;
    QMap<QRgb, int> m_avgDB;



};

#endif // TILECONVERTER_H
