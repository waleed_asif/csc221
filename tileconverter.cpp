#include "tileconverter.h"

TileConverter::TileConverter()
{
    reset();
    //ScanImage();

}
TileConverter::TileConverter(const TileConverter &params)
    : m_bestTile(params.m_bestTile),
      m_origImage(params.m_origImage),
      m_outImage(params.m_outImage),
      m_FilePathDB(params.m_FilePathDB),
      m_avgDB(params.m_avgDB)
{

}
TileConverter&
TileConverter::operator=(const TileConverter &params){
    if(&params == this)
        return *this;
    m_bestTile = params.m_bestTile;
    m_origImage = params.m_origImage;
    m_outImage = params.m_outImage;
    m_FilePathDB = params.m_FilePathDB;
    m_avgDB = params.m_avgDB;
    return *this;

}

void TileConverter::reset()
{
    m_bestTile = QImage();
    m_origImage = QImage();
    m_outImage = QImage();
    m_FilePathDB = QMap<QString, int>();
    m_avgDB = QMap<QRgb, int>();
}


void TileConverter::setOriginalImage(const QImage &image)
{

    m_origImage = image;
}

void TileConverter::setImage(const QImage &image)
{
    m_outImage = image;
}

const QImage& TileConverter::originalImage() const
{
    return m_origImage;
}

const QImage& TileConverter::tiledImage() const
{
    return m_outImage;
}

void TileConverter::computeAvgTile()
{
    QDir imagesDirectory("/home/waleed/practice/P1/Project1_6/tools");
    QDirIterator iterator(imagesDirectory, QDirIterator::NoIteratorFlags);
    int count = 0;
    for (; iterator.hasNext(); iterator.next())
    {
        QString imageName = iterator.filePath();
        if(iterator.fileName().contains("A-.375-VG"))
        {
            QImage tiles(imageName);
            QImage newTile = tiles.scaled(1,1, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
            QRgb tilePixel = newTile.pixel(0,0);
            m_FilePathDB[iterator.filePath()] = count;
            m_avgDB[tilePixel] = count;
            count++;
        }
    }
}

const QImage& TileConverter::ScanImage(const QImage &image)
{
    computeAvgTile();
    int inputImgWidth   = m_origImage.width();
    int inputImgHeight  = m_origImage.height();
    int newTileWidth    = inputImgWidth/187; //3/8" mosaic tiles will take 187 subregions in 70" Photo-Mosaic
    int newTileHeight   = inputImgHeight/187;
    int newImgWidth     =

}
